from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import ScheduleModel
from .forms import ScheduleForm

page_title = ""

def about(request):
    page_title = "About Ama"
    response = {"title" : page_title}
    return render(request, 'about.html', response)

def get_schedule(request):
    page_title = "Ama's Schedule"
    events = ScheduleModel.objects.all().values().order_by('-date', '-time')

    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            event = ScheduleModel()
            event.selection = form.cleaned_data['event_type']
            event.name = form.cleaned_data['event_name']
            event.time = form.cleaned_data['event_time']
            event.date = form.cleaned_data['event_date']
            event.place = form.cleaned_data['event_place']
            event.save()
            return HttpResponseRedirect('/schedule')
    else:
        form = ScheduleForm()

    response = {
        "title" : page_title,
        'form' : form,
        'events' : events,
    }
    return render(request, 'schedule.html', response)

def del_schedule(request):
    events.objects.all().delete()
    return render(request, 'schedule.html')
