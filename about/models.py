from django.db import models

# Create your models here.

SELECTION = (
    ("tugas", "Tugas"), 
    ("lab", "Lab"), 
    ("kuis", "Kuis"), 
    ("kepanitiaan", "Kepanitiaan"), 
    ("istirahat", "Istirahat"),
)

class ScheduleModel(models.Model):
    name = models.CharField(max_length=100)
    selection = models.CharField(max_length=5, choices=SELECTION)
    time = models.TimeField()
    date = models.DateField()
    place = models.CharField(max_length=50)