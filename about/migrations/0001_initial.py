# Generated by Django 2.1.1 on 2018-10-04 07:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ScheduleModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('selection', models.CharField(choices=[('TG', 'Tugas'), ('LB', 'Lab'), ('KS', 'Kuis'), ('PN', 'Kepanitiaan'), ('IS', 'Istirahat')], max_length=5)),
                ('time', models.TimeField()),
                ('date', models.DateField()),
                ('place', models.CharField(max_length=50)),
            ],
        ),
    ]
