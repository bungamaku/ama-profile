from django.urls import path

from .views import about, get_schedule, del_schedule

urlpatterns = [
    path('', about, name=''),
    path('about/', about, name='about'),
    path('schedule/', get_schedule, name='schedule'),
    path('delete/', del_schedule, name='delete'),
]