from django import forms
from .models import SELECTION

class ScheduleForm(forms.Form):

    event_type = forms.ChoiceField(choices=SELECTION, widget=forms.Select(attrs={
        'class': 'form',
        'required': 'True',
        'type': 'select',
    }))
    event_name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form',
        'required': 'True',
        'placeholder': "Event's name",
    }))
    event_time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class': 'form',
        'required': 'True',
        'type': 'time',
    }))
    event_date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form',
        'required': 'True',
        'type': 'date',
    }))
    event_place = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form',
        'required': 'True',
        'placeholder': "Event's place",
    }))